$(document).ready(function() {
	recentHash = "";
	function hashPoll(){
		var hash = window.location.hash.substr(1);
		if (hash==recentHash) {
			return; // Nothing's changed since last polled.
		}
		if(hash==""){
			$('#g_body').load("index.html #g_body");
			$('#links').fadeIn();
			$('#wrapper').hide();
		} else {
			var toLoad = hash+'.html #g_body';
			$('#g_body').load(toLoad);
			$('#links').hide();
			$('#wrapper').fadeIn();

			
		}
		recentHash = hash;
	}
	hashPoll();
	
	$('#g_sidebar a.dynamic, .three-nav a').click(function(){
		function loadContent() {
			$('#g_body').load(toLoad,'',showNewContent());
		}
		function showNewContent() {
			
			$('#g_body').fadeIn('normal',hideLoader());
		}
		function hideLoader() {
			$('#load').fadeOut('normal');
		}	
        var href = $(this).attr('href');
        if(href == "https://www.regattacentral.com/clubs/?org_id=2041") { return true; }
		if(window.location.hash.substr(1)!=href.substr(0,href.length-5)){	  
		    var toLoad = $(this).attr('href')+' #g_body';	
		    $('#load').remove();
		    $('#sidebar').append('<span id="load">LOADING...</span>');
		    $('#load').fadeIn('normal');
		    loadContent();
		    window.location.hash = $(this).attr('href').substr(0,$(this).attr('href').length-5);	    
		}
		return false;
		
	});



	setInterval(hashPoll, 500);
});
