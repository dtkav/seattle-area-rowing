					<div id="menu">
						<h2>DATES</h2>
						<div class="info">
							<p>
								<strong>Seattle Area Rowing Junior Women's Camp</strong><br />
								June 20th &ndash; July 13th, 2016 / July 18th (Racing Team)<br />
								Schedule: <br />
                                <strong>FIRST WEEK: June 20&ndash;24 is from 3:40pm&ndash;6:30pm</strong> <br />
                                <strong>AFTER JUNE 25TH: 8am&ndash;12pm</strong> <br />
                                Monday through Saturday 8am&ndash;12 PM,  Sunday: Off<br />
								Kick-Off meeting, Sunday June 19th, 2016 at 4pm at the Conibear Shellhouse <br /><br />
							</p>
						</div>
						<h2>DESCRIPTION</h2>            
						<div class="info">
							<p>
								The <strong>Seattle Area Rowing Junior Women's Camp</strong> is a day camp designed for female rowers and male or female coxswains that are looking to increase their speed, technical skills, training knowledge, and racing experience in a highly competitive environment.
							</p>
							<p>
								Athletes will train on the water for 2-3 hours with the <a href="http://www.seattlearearowing.org/padraicmcgovern.html">elite-level coaches</a> in eights and coxed fours. After the water workout, athletes will continue to improve their fitness on land using ergometers, strength training, core routines, and enhance their technical knowledge with video analysis. 
							</p>

							<p>
								The quality of both on and off the water coaching will ensure that every rower finishes the camp having reached a new level in their rowing or coxing ability. At the end of the three-week camp, nine athletes (eight rowers and one coxswain) will be selected to represent Seattle Area Rowing Camp at Harsha Lake in Bethal, OH.
							</p>
						</div>
						<h2>SETTING</h2>  
						<div class="info">
							<p>
								The camp takes place at the storied <a href="http://www.gohuskies.com/ViewArticle.dbml?ATCLID=208568061">Conibear Shellhouse</a>, on the edge of the University of Washington campus.
							</p>
							<p>
								Athletes will utilize top quality equipment and have access to the same facilities reserved for the University’s varsity program. Athletes will have access to locker rooms and all training facilities at the Conibear Shellhouse.
							</p>
							<p>
								Because of its ideal location, launching from the Conibear Shellhouse will allow access to multiple locations. Crews may find themselves taking part in morning rows on Lake Washington or doing race pieces through the famous <a href="http://www.gohuskies.com/ViewArticle.dbml?DB_OEM_ID=30200&ATCLID=208568762">Montlake Cut</a>.
							</p>
							<p>
								Longer technical sessions may take the boats onto Lake Union and down the shipping canal to the Government Locks. With many options to choose from, the boats will be able find good water in almost any weather conditions.
							</p>
						</div>
						<h2>FOOD</h2>  
						<div class="info">
							<p>
								Campers will be responsible for providing their own breakfasts, snacks, lunches and dinners during daily practice and team trips.
							</p>
						</div>
						<h2>TEAM TRIPS</h2>  
						<div class="info">
							<p>
                     The Club Nationals Racing Team of eight athletes and one coxswain will fly to the <a href="http://www.usrowing.org/events_new/details/2016/07/13/default-calendar/2016-usrowing-club-national-championships">USRowing Club National Championship</a> in Harsha Lake, OH. More details will be delivered during camp. 
							</p>
							<p>
						</div>
						<h2>TRAINING GEAR</h2>  
						<div class="info">
							<p>
								All athletes will receive a Seattle Area Rowing Unisuit, hat and dry fit t-shirt.  Please make sure to mark down your correct sizes when you register for the camp.
							</p>
						</div>
						<h2>OUT-OF-TOWN ATHLETE</h2>
						<div class="info">
							<p>
                        While we welcome out-of-town rowers and coxswains, Seattle Area Rowing will not provide housing or meals for any athlete. Those arriving in Seattle to participate in the camp must have their own transportation and housing arranged, and have someone that will be responsible for them during non-camp hours.

							</p>
							<p>
                        The Conibear Shellhouse is located in the University District of Seattle. Surrounding neighborhoods include (from nearest to farthest): Montlake, Laurelhurst, Ravenna, Bryant, Wallingford, Fremont, and Green Lake. Bus transportation is available, though may be unreliable for morning practices. The boathouse is also just off the Burke-Gilman Trail, a paved bike path.
							</p>							<p>
                        University of Washington Conference Services has agreed to offer summer housing through its Visiting Scholar Program for athletes who are 18 years old by the start of camp. This might be a good option for athletes who plan to attend UW in the fall, as it provides an opportunity to become familiar with the campus and community before classes begin. Contact Conference Services at confhous@hfs.washington.edu or at (206) 221-0597 for pricing information. Please note that SARA is not responsible for such athletes living in the dorms during non-camp hours.
							</p>
						</div>
						<h2>WAIVERS </h2>
						<div class="info">
							<p>
                     All athletes are required to download and complete a Health History, Medication, and Liability Release form, located in the right column of this page. You are required to bring the completed forms with you to the athlete check-in on the first day of camp. Athletes will not be allowed to practice until we have received all of the completed forms.  
							</p>
						</div>
                        <h2>KICK&ndash;OFF</h2>
                        <div class="info">
                            <p>
                            The kick-off meeting will be on Sunday June 19th at 4 PM in the Windermere Dining Room, located on the first floor of the Conibear Shellhouse. Parking is free on Sunday in the E8 lot, immediately north of the boathouse. Please bring all forms to this meeting.
                            </p>
                        </div>
						<h2>REGISTRATION </h2>  
						<div class="info">
							<p>
								All athletes can register and pay for the Racing Camp online at <a href="https://www.regattacentral.com/clubs/?org_id=2041">[regattacentral.com]</a>. They will also be prompted to input their sizes for camp gear at this time.
							</p>
						</div>
						<h2> COST </h2>  
						<div class="info">
							<p>
                     We are happy to announce a lower cost to this year's Seattle Area Rowing camp! In an effort to provide more opportunity to those athletes dedicated to improving their rowing, we have lowered the registration cost by 25 percent. 
							</p>
							<p>
                     Seattle Area Rowing Junior Women's Camp &mdash; $1,280 <br \>
                     Club Nationals Racing Team &mdash; $800 (select athletes)
							</p>
							<p>
                     Camp fees include all training and gear expenses. Club Nationals expenses cover round-trip flight, hotel room, and regatta fees. 
							</p>
						</div>
						<h2>Questions?</h2>  
						<div class="info">
							<p>
                     Feel free to email us at SeattleAreaRowingCamp@gmail.com.   
							</p>
						</div>
					</div>
