<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> 
	<meta name="keywords" content="seattle area northwest development camp rowing 2010">

	<title>Seattle Area Rowing</title>
	<link href="css.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<comment><script type="text/javascript" src="js.js"></script></comment>
	
	<!-- analytics -->
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-27732723-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body>
	<div id="container">
		<div id="header">
			<a href=".\"><img src="/logo.png" alt="logo" width="600" height="204"></a>
		</div>
		<div id="wrapper">
			<div id="main-content">
				<div id='g_body'>
