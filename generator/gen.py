#!/usr/bin/env python

import os
import shutil

header = open('header.php')
footer = open('footer.php')

try:
    shutil.rmtree('www')
except OSError:
    print "error removing www, probably didn't exist"

os.mkdir('www')


files = os.listdir('.')
for i in files:
    if (os.path.isfile(i) and i.endswith('.h')):
        f = open(i + 'tml', 'w')
        f.close()
        f = open(i + 'tml', 'a')
        g = open(i)
        f.write(header.read())
        f.write(g.read())
        f.write(footer.read())
        f.close()
        g.close()
        header.seek(0)
        footer.seek(0)
        shutil.move(i + 'tml', 'www/')
    elif (os.path.isfile(i) and i.endswith('.html')):
        shutil.copy(i, 'www/')
