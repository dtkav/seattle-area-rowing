					<h2>Bill Clifford</h2>
<table cellpadding="0" cellspacing="0" >
        <tbody>
            <tr>
                <td width="100" >2012-Present</td>

                <td >Varsity men&rsquo;s coach, Pocock Rowing Center</td>
            </tr>

            <tr>
                <td >2009-2012:</td>

                <td >Varsity men&rsquo;s coach, Everett Rowing Association</td>
            </tr>

            <tr>
                <td >2011-2012</td>

                <td >3rd place, Junior men 8+ Head of the CharlesSilver, Varsity men 8+ NW Regional Championships<br>
5th place Varsity men 8+ US Rowing National Championships<br>Gold, Ltwt men 8+ NW Regional Championships <br>
 9th place Ltwt men 8+, Youth National Championships<br>Gold , Varsity men 2- NW Regional Championships</td>
            </tr>

            <tr>
                <td >2010-2011</td>

                <td >
 2nd place, Junior men 8+ Head of the Charles<br>Gold, Varsity men 8+ NW Regional Championships<br>
 9th place Varsity men 8+ US Rowing National Championships<br>Gold, Ltwt men 8+ NW Regional Championships<br>
 9th place Ltwt men 8+, Youth National Championships<br>Gold , Varsity men 2- NW Regional Championships
</td>
            </tr>

            <tr>
                <td >2009-2010</td>

                <td >3rd place, Junior men 8+ Head of the Charles<br>Gold, Ltwt men 8+ NW Regional Championships <br>Gold , Varsity men 2- NW Regional Championships</td>
            </tr>

            <tr>
                <td >2001-2003</td>

                <td >San Diego State University Men&rsquo;s Rowing-Team Co-Captain/ Vice President</td>
            </tr>

            <tr>
                <td >1999-2001</td>

                <td >Orange Coast College-Varsity Captain</td>
            </tr>

            <tr>
                <td >1996-1999</td>

                <td >Everett Rowing Association-4 year rower-Varsity Captain</td>
            </tr>
        </tbody>
    </table>
    <p>"'Train smarter! Understand your goal!' That is my coaching philosophy. My goal is to not only make my rowers the fastest at our races but also the smartest. I want them to understand why we do what we do. I want them to understand what it takes to achieve their goals. It is my job to make them great rowers. I take it upon myself to make them more. I want my oarsmen to be great all around athletes as well as great rowers."
    </p>
    <p>
    -Currently competes in Iron Distance Triathlon, Ultra marathons, and ultra-distance cycling events.
    </p>
    <p>
    -Avid mountaineer and climber.
    </p>
