					<h2>Conor Bullis</h2>
					<p><strong> Head Coach, Lakeside School Men&#8217;s Crew </strong></p>
					<table>
						<tr>
							<td width="100">
								2011-Present
							</td>
							<td>
								Women&#8217;s Assistant/Freshmen coach, University of Washington
							</td>
						</tr>
						<tr>
							<td> 
								2008-2011
							</td>
							<td> 
								Head Coach, Lakeside School Men&#8217;s Crew
							</td>
						</tr>
						<tr>
							<td> 
								 2008-2009
							</td>
							<td> 
								 Varsity Assistant Coach, University of Washington
							</td>
						</tr>
						<tr>
							<td> 
								 2007-2008
							</td>
							<td> 
								 Freshmen Assistant Coach, University of Washington <br />
								 Coach of National Champion Freshmen Men&#8217;s 4+ <br />
								 IRA Course Record Holders
							</td>
						</tr>
						<tr>
							<td> 
								 2006-2007
							</td>
							<td> 
								 Freshmen Assistant Coach, Oregon State University 
							</td>
						</tr>
					</table>
