					<h2>Padraic McGovern</h2>
    <table cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td width="100">2008-Present</td>

                <td>Varsity girls coach, Everett Rowing Association</td>
            </tr>

            <tr>
                <td>2007-Present</td>

                <td>President, Kroll McGovern Construction Inc.</td>
            </tr>

            <tr>
                <td>2012</td>

                <td>Bronze, Girls 4+ Youth National Championships<br>8th place, Girls 2- Youth National Championships</td>
            </tr>

            <tr>
                <td>2005-2006</td>

                <td>Varsity Girls Coach, Everett Rowing Association</td>
            </tr>

            <tr>
                <td>2006</td>

                <td>Bronze, Girls 8+ Youth National Championships</td>
            </tr>

            <tr>
                <td>2005</td>

                <td>Silver, Girls 8+ Youth National ChampionshipsBrentwood Girls Points Trophy</td>
            </tr>

            <tr>
                <td>2002-2006</td>

                <td>Assistant Coach, Everett Rowing Association</td>
            </tr>

            <tr>
                <td>2004</td>

                <td>4th place, Girls Lt 8+, Youth National Championships</td>
            </tr>

            <tr>
                <td>1996-2002</td>

                <td>Lake Washington Rowing Club Competitive Sculling Team</td>
            </tr>

            <tr>
                <td>1992-1996</td>

                <td>Saint Mary&rsquo;s College of California<br>
4 year Letterman<br>
3 year Most Valuable<br>
1 year Best Oarsman</td>
            </tr>
        </tbody>
    </table>

