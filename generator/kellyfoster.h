					<h2>Kelly Foster</h2>
					<p><strong> Assistant Varsity Coach,  Lakeside School Men&#8217;s Crew </strong></p>
					<table>
						<tr>
							<td width="100">
								 2011-Present
							</td>
							<td>
								 Head Boys Coach, Sammamish Rowing Association
							</td>
						</tr>
						<tr>
							<td> 
								 2009-2011
							</td>
							<td> 
								 Lakeside Assistant Varsity Men's coach 
							</td>
						</tr>
						<tr>
							<td> 
								 2007
							</td>
							<td> 
								 Coach for Lakeside Educational Enrichment Program
							</td>
						</tr>
						<tr>
							<td> 
								 2005-2008
							</td>
							<td> 
								 Rowed for University of Washington
							</td>
						</tr>
						<tr>
							<td> 
								 2008
							</td>
							<td> 
								 2nd Place team at NCAA Championships
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 Won San Diego Crew Classic, Women's Varsity 8
							</td>
						</tr>             
						<tr>
							<td> 
								 2004-2005
							</td>
							<td> 
								 Rowed for Stanford University
							</td>
						</tr>
						<tr>
							<td> 
								 2003
							</td>
							<td> 
								 Sportswoman of the year for Snohomish County
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 Member of Junior National Team W8
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 silver medal at Junior World Rowing Championships in W8
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 Member of United States Indoor Rowing Team 
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 Won Indoor World Championships (CRASHB'S) for Junior Women, broke 2k American record for junior women 
							</td>
						</tr>
						<tr>
							<td> 
								 
							</td>
							<td> 
								 Won European Indoor Championship for Junior Women, broke course record
							</td>
						</tr>
						<tr>
							<td> 
								 2000-2004
							</td>
							<td> 
								 Rowed for Everett Rowing Association
							</td>
						</tr>
					</table>
