					<div id="menu">
						<h2>SCHEDULE</h2>
						<div class="info">
							<p>
								<strong>UW Winter Training Camp for Juniors</strong><br />
							</p>
							<p>
								<strong>Camp Schedule*</strong><br />
								Date: Satureday, 12/5/2015<br />
								Time: <br />
                                <ul>
                                    <li>9am &ndash; Sign in</li>
                                    <li>10am &ndash; Tour of Conibear Shellhouse</li>
                                    <li>10:30am &ndash; Row #1</li>
                                    <li>12:30pm &ndash; Video Review/Lunch</li>
                                    <li>1:30 pm &ndash; Row #2</li>
                                    <li>4pm &ndash; Camp Dismissed</li>
                                </ul>
							    <p>
                                    * In cases of bad weather, the rowing sessions could become weight room sessions in the Husky Weight room or RP3/video sessions. In all scenarios, the UW Coaching staff will be present and will instruct the camp attendees
							    </p>
							</p>
						</div>
						<h2>WHO WE ARE IS WHY WE WIN</h2>
						<h3>Get a Taste of UW Athletics</h3>
						<div>
							<p>
                                Being a part of the University of Washington Rowing Team is a special experience. The University is surrounded by water, the Conibear Shellhouse is on campus, and the Husky Crew can row all year round. Our Student Athletes thrive both on the water and in the classroom. 
							<p>
						</div>
						<h3>Meet the UW Coaching Staff</h3>            
						<div>
							<p>
                                There are few coaches in collegiate athletics whose accomplishments mirror those of Bob Ernst's. Ernst has guided teams to eight National Championships, one Olympic gold medal and numerous wins both domestically and abroad. Ernst has also developed & guided hundreds of rowers to the elite levels of their sport.
							</p>
							<p>
                                In his seventh year at Washington and his fifth on the Women’s team as the first assistant and recruiting coordinator, Conor Bullis has been bringing in the top talent from the Northwest, the United States and from around the world and coaching his crews to three straight Pac-12 Championships.
							</p>
							<p>
                                Alan Meininghaus joins the UW Women’s Coaching Staff this year. As a student athlete at the University of Washington, Alan won two Gold Medals at the IRA National Championships. Rowing in the Men’s Open 4+ in 2009, and Men’s 2nd Varsity 8+ in 2012. Last year Alan coached the men’s 4+ to a gold medal at the IRA. 
							</p>
							<p>
                                Ben Hise Joins the UW Coaching staff as the intern assistant coach after coaching at both the Newport Aquatic Center and UC Irvine in Newport Beach, California.
							</p>
						</div>
						<h3>Row at the Conibear Shellhouse:</h3>
						<div>
							<p>
                                The Conibear Shellhouse is located on the shores of Lake Washington and has served as the primary home for Husky Crew since its construction in 1949. A complete renovation in 2005 increased space by 75 percent to 47,250 square feet. The Conibear Shellhouse can be described as the heartbeat of athletic life as it serves as a hub for all of our Husky student-athletes. Both the men’s and women’s crew locker rooms are housed in Conibear Shellhouse, just steps from the water. 
							</p>
						</div>
						<h2>HOW TO REGISTER:</h2>
						<div>
							<p> 
                                registration is open at <a href="https://regattacentral.com">regattacentral.com</a>
							</p> 
						</div>
					</div>
