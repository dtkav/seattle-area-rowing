				</div>
			</div>
			<div id="sidebar">
				<div class="links">
					<div id='g_sidebar'>
						<h1><a href="uw_camp.html" class="dynamic">UW Winter Training Camp for Juniors</a></h1>
						<h1><a href="m_racingcamp.html" class="dynamic">Junior Men's Racing Camp</a></h1>
						<h1><a href="w_racingcamp.html" class="dynamic">Junior Women's Racing Camp</a></h1>
						<h1>About Us</h1>
						<ul>
							<li><a href="padraicmcgovern.html" class="dynamic">Padraic McGovern</a></li>
						</ul>
					</div>
					<h1>Forms</h1>
					<div id="forms">
						<ul>
							<li><img src="pdf.jpg" height="16px;" class="ico" /><a href="http://seattlerowing.s3.amazonaws.com/Seattle Camp Waiver Form (Revised 3-4-10).doc"> Release of Liability</a></li>
							<li><img src="pdf.jpg" height="16px;" class="ico" /><a href="http://seattlerowing.s3.amazonaws.com/Washington Rowing Camp Medication Form (Revised 3-4-10).doc"> Medication Form</a></li>
							<li><img src="pdf.jpg" height="16px;" class="ico" /><a href="http://seattlerowing.s3.amazonaws.com/Seattle Area Rowing Camps Health History and Examination Form (Revised 3-4-10).doc"> Health History Form </a></li>
						</ul>                               
					</div> 
				</div>
			</div>
			<div id="footer"></div>
		</div>
	</div>
	<div id="foot_space"></div>
</body>
</html>

